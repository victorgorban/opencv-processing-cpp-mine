#pragma once

// не могу понять как скомпилить одновременно для мака и винды. Так что придется иметь разные исполняемые файлы для разных платформ.

#include <iostream>
#include <fmt/core.h>
#include <cpr/cpr.h>
#include <string.h>
#include <ctype.h>
#include <chrono>
#include <format>
#include <nlohmann/json.hpp>

using namespace nlohmann::literals; // R"..."__json;
using json = nlohmann::json;
namespace fs = std::filesystem;

namespace helpers
{

    json submitObject(const std::string &url, json &objectData)
    {
        cpr::Response r = cpr::Post(cpr::Url{url},
                                    cpr::Body{objectData.dump()},
                                    cpr::Header{{"Content-Type", "application/json"}});
        // std::cout << "Status code: " << r.status_code << '\n';
        // std::cout << "Request took " << r.elapsed << std::endl;
        if (r.status_code < 200 || r.status_code >= 400)
        {
            throw std::runtime_error(r.error.message);
        }
        // std::cout << "Text: " << r.text << '\n';

        json parsedResponse = json::parse(r.text);
        // std::cout << "parsedResponse status " << parsedResponse["status"].back() << std::endl;
        if (parsedResponse["status"].back() == "error")
        {
            throw std::runtime_error(parsedResponse["message"].back());
        }
        return parsedResponse;
    }

    std::string getExePathWindows()
    {
        char result[MAX_PATH];
        return std::string(result, GetModuleFileName(NULL, result, MAX_PATH));
    }

    std::string readFileText(fs::path path)
    {
        // Open the stream to 'lock' the file.
        std::ifstream f(path, std::ios::in | std::ios::binary);
        if (!f.good())
        {
            return "";
        }

        // Obtain the size of the file.
        const auto sz = fs::file_size(path);

        // Create a buffer.
        std::string result(sz, '\0');

        // Read the whole file into the buffer.
        f.read(result.data(), sz);
        f.close();

        return result;
    }

    void rewriteFileText(fs::path path, std::string contents)
    {
        std::ofstream ofs(path, std::ofstream::trunc);

        ofs << contents;

        ofs.close();
    }

}