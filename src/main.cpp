#include <iostream>
#include <fmt/core.h>
#include <cpr/cpr.h>
#include <opencv2/opencv.hpp>
#include "opencv2/core/utility.hpp"
#include "opencv2/video/tracking.hpp"
#include <opencv2/video/background_segm.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <string.h>
#include <chrono>
#include <format>
#include <ctype.h>
#include <nlohmann/json.hpp>

// включать напрямую .cpp файлы это считается плохим тоном,
// но делать компилятор с которым нужно долбаться полгода и писать одно и то же в 2х файлах еще хуже.
#include "./helpers.cpp"
#include "./programParts.cpp"

using namespace nlohmann::literals; // R"..."__json;
using json = nlohmann::json;
using namespace cv;
namespace fs = std::filesystem;

using namespace helpers;

fs::path exeName, exeFolder;
json jsonSettings = {};
std::fstream settingsStream;
Mat sourceImage, outputImage;
Mat pathLayer;
bool isAreaSelecting = false;
Point selectionOrigin;
Rect selection(322, 221, 1275, 643);                                        // Rect selection;
Ptr<BackgroundSubtractor> pBgSubtractor = createBackgroundSubtractorMOG2(); // создался с дефолтными параметрами
VideoWriter videoWriter;

std::string selectedVideoSource = "0";
std::string selectedSaveFolder;
VideoCapture videoCapture;

Mat currentFrame;
bool isProcessing = false;

std::string hot_keys =
    "\n\nHot keys: \n"
    "\tESC - exit the program\n"
    "\tr - reinitialize the camera\n"
    "\tSpace - start/stop the processing\n"
    "\tc - cancel rectangle selection\n"
    "To initialize processing, select the table playing area with mouse\n";

// todo очень просится файл-хелпер, чтобы засунуть туда все вспомогательные функции
// универсальный хелпер
json submitObject(const std::string &url, json &objectData);
// эту функцию очень неудобно выделять в отдельный файл. Потому что изменяет очень много глобальных переменных.
// Можно использовать объявления extern, но это запутает программу еще больше.
void handleSourceImageMouse(int event, int x, int y, int, void *);
extern VideoCapture openVideoCapture(std::string &);

void startProcessing()
{
    // начало обработки - значит открываем файл для сохранения видео
    // нашел еще одну причину когда videoWriter не открывается. Он не поддерживает пробелы в пути или названии файла. Что за бред, почему об этом не сказано в доках?
    // ага, двоеточия тоже не работают.
    std::string dateString = std::format("{:%F-%H-%M-%S}", std::chrono::system_clock::now());
    std::string saveFileName = selectedSaveFolder + "\\" + dateString + ".mp4";
    std::cout << "startProcessing, saveFileName " << saveFileName << std::endl;
    std::cout << "startProcessing, fps and fourcc " << videoCapture.get(CAP_PROP_FPS) << " " << VideoWriter::fourcc('m', 'p', '4', 'v') << std::endl;
    // VideoWriter (const String &filename, int fourcc, double fps, Size frameSize, bool isColor=true)
    videoWriter.open(
        saveFileName,                              // filename
        (double)videoCapture.get(CAP_PROP_FOURCC), // все это не работает. Ни один кодек.
        // VideoWriter::fourcc('m', 'p', '4', 'v'), // все равно не открывается. Нет текста ошибки. Как мне это отлаживать?
        // Попробовал несуществующий у меня кодек - показывает ошибку что либа не найдена. Но с этими-то такого нет, с ними что?
        videoCapture.get(CAP_PROP_FPS) || 60, // fps это свойство есть только у видеофайлов. Для камер нужно вычислять самому.
        Size(                                 // frameSize
            (int)videoCapture.get(CAP_PROP_FRAME_WIDTH),
            (int)videoCapture.get(CAP_PROP_FRAME_HEIGHT)),
        true); // isColor
    if (!videoWriter.isOpened())
    {
        throw std::runtime_error("Cannot open the videoWriter"); // и никакой ошибки блин. Никакой информации.
    }
    isProcessing = true;
    std::cout << "startProcessing, isProcessing true " << std::endl;
}

// todo надо уже закончить обработку. Логика довольно простая.

void stopProcessing()
{
    isProcessing = false;
    if (videoWriter.isOpened())
    {
        std::cout << "releasing videoWriter " << std::endl;
        videoWriter.release();
    }
}

void triggerProcessing()
{
    std::cout << "In triggerProcessing " << isProcessing << std::endl;
    if (!isProcessing)
    {
        startProcessing();
    }
    else
    {
        stopProcessing();
    }
}

void parseUserInput(char c)
{
    switch (c)
    {
        // Space - start or stop the processing
    case 32:
        std::cout << "pressed space" << std::endl;
        triggerProcessing();
        break;
        // c - cancel rectangular selection
    case 'c':
        selection &= Rect(0, 0, 0, 0);
        break;
        //@ - enter camera number and reinit
    case 'r':
        // reinit camera
        if (!reinitVideoSource(selectedVideoSource))
        {
            std::cout << "Cannot access the camera. Try again." << std::endl;
        }
        break;
    default:;
    }
}

Mat processVideoFrame(Mat &frameToProcess)
{
    Mat processedFrame = frameToProcess;
    // frame точно уже не пустой, т.к. попали сюда
    // auto smallFrame = processedFrame.getRegion(selection); // Создаем новую матрицу c обрезанным размером - rect
    Mat smallFrame = processedFrame(selection); // в 4.x вместо getRegion используется такой синтаксис

    Mat foreGroundMask;
    pBgSubtractor->apply(smallFrame, foreGroundMask); // написано что .apply принимает 2 аргумента, 2-й аргумент это маска Mat. https://docs.opencv.org/4.x/d1/dc5/tutorial_background_subtraction.html

    Mat blurred, thresholded, eroded;
    // здесь везде синтаксис: src, dest, ...args
    blur(foreGroundMask, blurred, Size(2, 2));                                      // берем источник, блюрим
    threshold(blurred, thresholded, 250, 255, THRESH_BINARY);                       // берем уже заблюренный и обрабатываем дальше
    erode(thresholded, eroded, getStructuringElement(MORPH_ELLIPSE, Size(26, 26))); // дальнейшая работа с thresholded.
    //  let thresholded = foreGroundMask
    //                 .blur(new cv.Size(2, 2))
    //                 .threshold(250, 255, cv.THRESH_BINARY)
    //                 .erode(cv.getStructuringElement(cv.MORPH_ELLIPSE, new cv.Size(26, 26)))

    Mat smallPathLayer = pathLayer(Rect(selection.x, selection.y, thresholded.cols, thresholded.rows));
    thresholded.copyTo(smallPathLayer); // Тут выбираем место вставки одного слоя видео на другой видео

    Mat newPathLayer;
    cvtColor(pathLayer, newPathLayer, COLOR_GRAY2BGR); // Меняем цветовую схему нового слоя с бинарного на цветную 3х канальную иначе не склеится

    cv::add(processedFrame, newPathLayer, processedFrame); // Накладываем на исходный кадр графический слой с траекторией

    return processedFrame;
}

bool showAndProcessVideoByFrame()
{
    videoCapture >> currentFrame;
    // пустой фрейм скорее всего значит что загнулась, или видео закончилось.
    if (currentFrame.empty())
    {
        // std::cout << "currentFrame is empty" << std::endl;
        stopProcessing();
        return false;
    }
    currentFrame.copyTo(sourceImage);
    // нарисовать прямоугольник - обозначить зону выделения

    // std::cout << "selection" << selection << std::endl;

    rectangle(sourceImage, selection, Scalar(255, 0, 0), 2);

    imshow("Source", sourceImage);

    // если размер прямоугольника не 0, то обработать кадр. И так далее.
    if (isProcessing)
    {
        Mat outputFrame = processVideoFrame(currentFrame);
        videoWriter.write(outputFrame); // Пишем результат в файл кадр за кадром,
        outputFrame.copyTo(outputImage);
        rectangle(outputImage, selection, Scalar(255, 0, 0), 2); // показать выделение для теста
        imshow("Output", outputImage);
    }
    else
    {
        // Если остановили обработку, наверное нужно сохранить файл. Но обработка останавливается по пробелу, поэтому не надо.
    }

    return true;
}

int main(int argc, char **argv)
{
    try
    {
        auto exePath = fs::path(getExePathWindows());
        exeName = exePath.filename();
        exeFolder = exePath.parent_path();
        std::string settingsText = readFileText(exeFolder / "settings.json");
        if (!settingsText.empty())
        {
        }
        else
        {
            settingsText = readFileText(exeFolder / "assets/settings-default.json");
        }
        jsonSettings = json::parse(settingsText);
        std::cout << "json parsed " << jsonSettings.dump(4) << " from directory " << exeFolder.string() << std::endl;

        std::string dateString = std::format("{:%F %T}", std::chrono::system_clock::now());
        std::cout << "date string " << dateString << std::endl;

        json loginResultData = loopLoginUntilSuccess(jsonSettings, exeFolder / "settings.json"); // {userId, token}. В теории можно сохранить где-то, но такой задачи не было

        // проверяет введенный источник через его открытие в videoCapture.
        // Так что лучше его сначала закрыть, потом опять открыть когда нужно
        selectedVideoSource = loopVideoSourceUntilSuccess(jsonSettings, exeFolder / "settings.json");
        selectedSaveFolder = loopSaveFolderPathUntilSuccess(jsonSettings, exeFolder / "settings.json");

        // Создаем контейнер для графического слоя который будем накладывать поверх видео, размером с кадр
        pathLayer = Mat(videoCapture.get(CAP_PROP_FRAME_HEIGHT), videoCapture.get(CAP_PROP_FRAME_WIDTH), CV_8UC1, Scalar(0, 0, 0)); //, [0]

        namedWindow("Source", 0);
        setMouseCallback("Source", handleSourceImageMouse, 0);
        namedWindow("Output", 0);

        showHelp(hot_keys);

        // startProcessing();
        // отрисовка фреймов в другом окне, плюс анализ ввода.
        for (;;)
        {
            showAndProcessVideoByFrame();

            char c = (char)waitKey(10);
            if (c == 27)
                break;
            parseUserInput(c);
        }

        return 0;
    }
    catch (std::exception &e)
    {
        std::cerr << "Runtime error occured: " << e.what() << std::endl;
    }
}

extern VideoCapture openVideoCapture(std::string &source)
{
    std::cout << "openVideoCapture, source" << source << std::endl;
    if (isdigit(source[0]))
    {
        int camNumber = source[0] - 48;
        // std::cout << "is digit" << camNumber << std::endl;
        videoCapture.open(camNumber);
    }
    else
    {
        // std::cout << "not is digit" << source[0] << std::endl;
        videoCapture.open(source);
    }

    return videoCapture;
}

// extern значит, что переменная объявлена "где-то".
// в данном случае это можно представить как соединение extern-ов. Это единственный современный язык,
// где для понимания приходится использовать не логику, а фантазию.
extern bool reinitVideoSource(std::string &source)
{
    openVideoCapture(source);
    if (!videoCapture.isOpened())
    {
        // std::cout << "Error: Camera " << num << " is not available. Please select another camera. \n";
        return false;
    }
    return true;
}

void handleSourceImageMouse(int event, int x, int y, int, void *)
{
    if (isAreaSelecting)
    {
        selection.x = MIN(x, selectionOrigin.x);
        selection.y = MIN(y, selectionOrigin.y);
        selection.width = std::abs(x - selectionOrigin.x);
        selection.height = std::abs(y - selectionOrigin.y);
        selection &= Rect(0, 0, sourceImage.cols, sourceImage.rows);
    }
    switch (event)
    {
    case EVENT_LBUTTONDOWN:
        selectionOrigin = Point(x, y);
        selection = Rect(x, y, 0, 0);
        isAreaSelecting = true;
        break;
    case EVENT_LBUTTONUP:
        isAreaSelecting = false;
        break;
    }
}
