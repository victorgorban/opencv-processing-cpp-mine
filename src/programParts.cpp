// зачем нужен #pragma once: потому что "гениальный" компилятор cpp по умолчанию тупо копирует содержимое любого файла который подключается #include.
// так вот #pragma once сигнализирует компилятору, что "если этот файл уже в приложении, не копируй его еще раз".
// По этой причине в .h файле приходится писать также список всех библиотек. А в одноименном .cpp файле включать .h файл.
// вроде бы есть специальные микропрограммки которые генерируют .h файл из готового .cpp файла,
// но они не поддерживают 100% возможностей cpp, поэтому приходится страдать еще и с этой файловой херней.
// еще раз. Я включаю .h файл библиотеки и в главной программе, и в .cpp файле библиотеки. Если не будет #pragma once, то будут ошибки компилятора.
#pragma once

#include <iostream>
#include <fmt/core.h>
#include <cpr/cpr.h>
#include <opencv2/opencv.hpp>
#include "opencv2/core/utility.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <string.h>
#include <ctype.h>
#include <nlohmann/json.hpp>
#include "./helpers.cpp"

using namespace nlohmann::literals; // R"..."__json;
using json = nlohmann::json;
namespace fs = std::filesystem;

using namespace helpers;

// extern значит, что переменная объявлена "где-то".
extern bool reinitVideoSource(std::string &);

std::string loopSaveFolderPathUntilSuccess(json &jsonSettings, fs::path settingsPath)
{
    std::string saveFolderPath = "";
    bool isTryingSavedSettings = false;
    bool isNoSavedSettings = jsonSettings["saveFolder"].is_null();
    if (!isNoSavedSettings)
    {
        isNoSavedSettings = !jsonSettings["saveFolder"].is_string();
        if (!isNoSavedSettings)
        {
            saveFolderPath = jsonSettings["saveFolder"].template get<std::string>();
            isNoSavedSettings = saveFolderPath.empty();
        }
    }

    if (isNoSavedSettings)
    {
        std::cout << "Enter the folder where to save the processed videos" << std::endl;
    }
    else
    {
        saveFolderPath = jsonSettings["saveFolder"].template get<std::string>();
        isTryingSavedSettings = true;
    }
    // тут нужна такая логика: если есть уже номре камеры и он работает, то вывести с чего будем считывать. Если нет, то попросить ввести его.
    while (true)
    {
        try
        {
            if (!isTryingSavedSettings)
            {
                getline(std::cin, saveFolderPath);
            }
            else
            {
                std::cout << "Trying to open the saved folder: " << saveFolderPath << std::endl;
                isTryingSavedSettings = false;
            }

            if (!fs::exists(saveFolderPath))
            {
                throw std::runtime_error("Cannot access the folder");
            }
            jsonSettings["saveFolder"] = saveFolderPath;
            std::cout << "OK" << std::endl;

            rewriteFileText(settingsPath, jsonSettings.dump(4));
            break;
        }
        catch (std::exception &e)
        {
            std::cerr << "Cannot access video source. " << e.what() << std::endl;
            std::cout << "Try to enter the video source again: ";
        }
    }
    return saveFolderPath;
}

std::string loopVideoSourceUntilSuccess(json &jsonSettings, fs::path settingsPath)
{
    std::string videoSource;
    bool isTryingSavedSettings = false;
    bool isNoSavedSettings = jsonSettings["videoSource"].is_null();
    if (!isNoSavedSettings)
    {
        isNoSavedSettings = !jsonSettings["videoSource"].is_string();
        if (!isNoSavedSettings)
        {
            videoSource = jsonSettings["videoSource"].template get<std::string>();
            isNoSavedSettings = videoSource.empty();
        }
    }

    if (isNoSavedSettings)
    {
        std::cout << "Enter the camera number or the video path. Default value is 0 (default webcamera)" << std::endl;
    }
    else
    {
        videoSource = jsonSettings["videoSource"].template get<std::string>();
        isTryingSavedSettings = true;
    }
    // тут нужна такая логика: если есть уже номре камеры и он работает, то вывести с чего будем считывать. Если нет, то попросить ввести его.
    while (true)
    {
        try
        {
            if (!isTryingSavedSettings)
            {
                getline(std::cin, videoSource);
            }
            else
            {
                std::cout << "Trying to open the camera with saved videoSource: " << videoSource << std::endl;
                isTryingSavedSettings = false;
            }

            // todo надо считывать строку. Если это номер, то сохранять номер. Если это строка-путь, то сохранять как путь к видео.

            bool isReinitOk = reinitVideoSource(videoSource);
            if (!isReinitOk)
            {
                throw std::runtime_error("Cannot open the camera");
            }
            jsonSettings["videoSource"] = videoSource;
            std::cout << "OK" << std::endl;

            rewriteFileText(settingsPath, jsonSettings.dump(4));
            break;
        }
        catch (std::exception &e)
        {
            std::cerr << "Cannot access the camera. " << e.what() << std::endl;
            std::cout << "Try to enter the camera number again: ";
        }
    }
    return videoSource;
}

// std::string& email означает "взять параметр по ссылке", это синтаксический сахар для указателей из C.
// Технически это все еще указатель, ссылку даже можно преобразовать через какой-то cast в указатель и обратно, и все будет работать.
// сахар заключается в том, что вызывается функция как обычно, и внутри функции работа пишется "как обычно". С указателями бы пришлось юзать синтаксис *email.
//
// вызов:
// loopLoginUntilSuccess(email) - функция loopLoginUntilSuccess(std::string& email)
// loopLoginUntilSuccess(&email) - функция loopLoginUntilSuccess(std::string* email)
// вообще эта логика "pass by reference" была создана для того чтобы отправлять примитивы в функцию, и внутри функции изменять их.
// Но внутри cpp все настолько может быть запутано скрытыми перегрузками и кастами
// (а CPP позволяет даже неявные преобразования из объекта в примитив и обратно, как тебе такое, Илон Маск?)
//, что лучше взять себе за правило:
// в функцию всегда отправлять параметры по ссылке, даже если тебе это не надо.
// в плане производительности нет разницы между отправкой примитива типа int и объекта (потому что объект и так передается по ссылке, а размер ссылки равен размеру int),
// но из-за дикой запутанности возможностей языка приходится следовать правилу что я описал.
json loopLoginUntilSuccess(json &jsonSettings, fs::path settingsPath)
{
    // тут нужна такая логика: если есть уже логин и пароль, то вывести с чем пытаемся залогиниться. Если нет, то попросить ввести повторно.
    std::string email, password;
    bool isTryingSavedSettings = false;
    bool isNoSavedSettings = jsonSettings["email"].is_null() || jsonSettings["password"].is_null();
    if (!isNoSavedSettings)
    {
        isNoSavedSettings = !jsonSettings["email"].is_string() || !jsonSettings["password"].is_string();
        if (!isNoSavedSettings)
        {
            email = jsonSettings["email"].template get<std::string>();
            password = jsonSettings["password"].template get<std::string>();
            isNoSavedSettings = email.empty() || password.empty();
        }
    }

    if (isNoSavedSettings)
    {
        std::cout << "To use the program, you need to login. Please enter your system email and password" << std::endl;
        std::cout << "email and password, divided by space: ";
    }
    else
    {
        email = jsonSettings["email"].template get<std::string>();
        password = jsonSettings["password"].template get<std::string>();
        isTryingSavedSettings = true;
    }

    json loginResult;

    bool isLoginSuccess = false;
    while (!isLoginSuccess)
    {
        try
        {
            if (!isTryingSavedSettings)
            {
                std::cin >> email >> password;
            }
            else
            {
                std::cout << "Trying to login with saved credentials: " << email << " " << password << std::endl;
                isTryingSavedSettings = false;
            }

            json loginData = {
                {"email", email},
                {"password", password}};

            loginResult = submitObject("https://performstars.com/api/user/login", loginData);
            isLoginSuccess = true; // Так как я выдаю исключение при наличии status:error уже в submitObject, то эта переменная тут не нужна, но пусть будет цикл - так понятнее задумка.
            jsonSettings["email"] = email;
            jsonSettings["password"] = password;
            std::cout << "OK" << std::endl;

            rewriteFileText(settingsPath, jsonSettings.dump(4));
        }
        catch (std::exception &e)
        {
            std::cerr << "email error: " << e.what() << std::endl;
            std::cout << "Try to email again: ";
        }
    }

    return loginResult["data"];
}

void showHelp(std::string &hot_keys)
{
    std::cout << hot_keys;
}
