/**
 * Created by stuur on 12.07.2023.
 */

let cap = new cv.VideoCapture(video.path); //Это вариант для работы с видео файлом. Для потока это не нужно

let rect = new cv.Rect(x, y, width, heigth) //Это прямоугольник обрезки стола -

//Создаём контейнер, в который будем писать выходной файл
write_land = new cv.VideoWriter('/home/stuurgurs/media/shotvideos/'+sid+'/shotvideoland.mp4', cv.VideoWriter.fourcc("mp4v"), video.fps,
        new cv.Size(video.width, video.height ), true );

//Создаем контейнер для графического слоя который будем накладывать поверх видео, размером с кадр
let path = new cv.Mat(video.height, video.width, cv.CV_8UC1, [0]);

if (!end) end = begin + video.fps * 10

let bgSubtractor = new cv.BackgroundSubtractorMOG2()

let frames = begin

cap.set(cv.CAP_PROP_POS_FRAMES, frames) //Эта функция перематывает видео файл на указанный кадр, Для потокового видео это не нужно, там начало
    //определяется текущим моментом времени

while (frames <= end) {  //Это ограничение по максимальному времени работы алгоритма, или до команды в потоковой версии

        let frame = cap.read(); //Читаем текущий кадр в видео - получаем матрицу изображения frame

        if (!frame.empty) {

            frames++; //Крутим счётчик кадров в ручную чтобы успель за cap.read() - он тоже крутит кадры на каждом вызове

            let smallFrame = frame.getRegion( rect ) //Создаем новую матциуцу c обрезанным размером - rect

            let foreGroundMask = bgSubtractor.apply(smallFrame); //Применяем функцию bgSubtractor

            let thresholded = foreGroundMask
                .blur(new cv.Size(2, 2))
                .threshold(250, 255, cv.THRESH_BINARY)
                .erode(cv.getStructuringElement(cv.MORPH_ELLIPSE, new cv.Size(26, 26)))

            //ground_port = ground_port.add(thresholded)

            // ground_port = ground_port.add(blackFrame)

            thresholded.copyTo(path.getRegion(new cv.Rect(rect.x, rect.y, thresholded.cols, thresholded.rows))) //Тут выбираем место вставки одного слоя видео на другой видео

            let newpath = path.cvtColor(cv.COLOR_GRAY2BGR) //Меняем цветовую схему нового слоя с бинарного на цветную 3х канальную иначе не склеится

            frame = frame.add(newpath) //Накладываем на исходный кадр графический слой с траекторией

            //frame = frame.rotate( cv.ROTATE_90_CLOCKWISE )  //Опционально, так можно сделать вертикльное видео для смартфона

            write_port.write( frame ) //Пишем результат в файл кадр за кадром,

        }

        else {
            cap.reset();
            break;
        }
    }

write_land.release()

